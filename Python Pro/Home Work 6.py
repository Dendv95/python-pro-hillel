class Student:

    grade = 'Student'

    def __init__(self, name, surname):
        self.name = name
        self.surname = surname


    def add_grade(self):

        grade_dict = {
            0: 'Student',
            1: 'Bachelor',
            2: 'Master',
            3: 'Doctor'
        }
        if self.grade not in grade_dict.values():
            self.grade = 'Student'
        for k, v in grade_dict.items():
            if self.grade == v:
                if k < 3:
                    k +=1
                    self.grade = grade_dict.get(k)
                    return self.grade
        return f'Your grade is {self.grade}. That grade is max'

    def display(self):
        return f'Grade of {self.name} {self.surname} is {self.grade}'


st1 = Student('Nick', 'Fury')
print(st1.grade)
print(st1.name)
print(st1.surname)
print(st1.display())
print(st1.add_grade())
print(st1.display())
st2 = Student('Alex', 'Bern')
st3 = Student('Jey', 'Rox')
print(st3.add_grade())
print(st3.add_grade())
print(st3.add_grade())
print(st3.add_grade())
print(st3.display())


class Group:

    qty = 0
    student_list = []

    def __init__(self, name):
        self.name = name

    def add_student(self, student):

        if isinstance(student, Student):
            self.student_list.append(student)
        self.qty = len(self.student_list)
        return self.student_list, self.qty


    def display_group(self):

        return f'In our group {self.name} are {self.qty} persons!'


gr1 = Group('A')
print(gr1.display_group())
print(gr1.add_student(st1))
print(gr1.add_student(st2))
print(gr1.display_group())
print(gr1.add_student(st3))
print(gr1.display_group())
for i in gr1.student_list:
    print(i.name, i.grade, i.surname)