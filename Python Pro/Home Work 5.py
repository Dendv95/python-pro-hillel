class Counter:

    def __init__(self, min_value = 0, max_value = 100, current_value=1):
        self.min_value = min_value
        self.max_value = max_value
        self.current_value = current_value

    def increase(self):
        if self.min_value <= self.current_value < self.max_value:
            self.current_value += 1
            return self.current_value
        else:
            print('Out of range')

    def get_current_value(self):
        return self.current_value


a = Counter (3, 10, 4)
print(a.min_value)
print(a.max_value)
print(a.current_value)
print(a.increase())
print(a.current_value)
print(a.increase())
print(a.get_current_value())
b = Counter()
b.current_value = 3
print(b.min_value)
print(b.max_value)
print(b.current_value)
print(b.increase())
print(b.current_value)
print(b.increase())
print(b.current_value)
print(b.get_current_value())
c = Counter()
print(c.min_value)
print(c.max_value)
print(c.current_value)
print(c.increase())
print(c.current_value)
print(c.increase())
print(c.current_value)
print(c.get_current_value())