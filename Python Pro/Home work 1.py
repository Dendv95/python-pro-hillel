#Home_work_1
from flask import Flask

app = Flask(__name__)

@app.route("/hello/flask/")
def hello_world():
    return "<p>Hello from Flask application</p>"

app.run(debug=True)