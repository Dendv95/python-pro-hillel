# Home Work 2\ Decorator

def print_log(function):
	"""
	Decorator
	:param function:
	:return: type str, confirmation that function was done.
	"""
	def wrapper(*args, **kwargs):
		print(f"{function.__name__} была исполнена")
		result = function(*args, **kwargs)
		return result
	return wrapper


@print_log
def some_func(x, y):
	"""
	:param x: type int\float, multiplier
	:param y: type int\float, multiplicanda
	:return: product of x * y
	"""
	return x * y


print(some_func(4, 6))
