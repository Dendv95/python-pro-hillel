import requests
import sqlite3
from DB_Utils import get_session
from model import BTC




class Bitcoin_rate:

    _currency = 'USD'

    def __init__(self, cur=None):
        if cur is not None:
            self.currency = cur

    @property
    def currency(self):
        return self._currency

    @currency.setter
    def currency(self, value):
        """
        Проверяет корректный ввод данных по валюте.

        Returns: устанаваливает наименование валюты введенное пользователем объекту класса.

        """
        if not isinstance(value, (str)):
            raise TypeError
        elif len(value) != 3:
            raise 'Please, use Currency abbreviation (three-letter code). For example "USD"'
        value = value.upper()
        self._currency = value


    def check_con(self, api):
        '''
          Функция для проверки связи с API. Используется в основных методах.

        '''
        try:
            res = requests.get(api)
        except:
            print('Some exception')
        else:
            if 300 > res.status_code >= 200 and res.headers['Content-Type'] == 'application/json; charset=utf-8':
                print('No exception')
                return res

    @staticmethod
    def write_file(date, list):
        '''
          Метод для записи актуального нкурса валют в отдельный файл.

        '''
        with open('exchange_btc', 'w') as file:
            file.write(date + '\n')
            file.writelines("%s\n" % m for m in list)

    def cur_rate(self):
        '''
        Метод для получения актуальной информации по курсу всех валют к BTC на сейчас.
        Использует def write_file(date, list):
        Returns: в отдельный файл записывает дату и курсы всех валют. Сообщает об этом пользователю.

        '''
        api = ('https://bitpay.com/api/rates')
        res = self.check_con(api)
        json_data = res.json()
        counter = 0
        currency_list = []
        actual_date = res.headers['Date']
        for i in json_data:
            counter += 1
            rate = i['rate']
            cur = i['code']
            answer = f'{counter}. {cur} to BTC: {rate}'
            currency_list.append(answer)
        self.write_file(actual_date, currency_list)
        return 'You can find rate of exchange in file "exchange_btc"'

    def user_request(self):
        '''
        Метод использует заданные пользователем объекту валюту и возвращает курс к биткоину.
        Returns: курс валюты к биткоину. Если такой валюты нет говорит 'Don't know'

        '''
        user_currency = self.currency
        self.api = ('https://bitpay.com/api/rates')
        res = self.check_con(self.api)
        json_data = res.json()
        list_of_cur = []
        for i in json_data:
            list_of_cur.append(i['code'])
            if i['code'] == user_currency:
                return f"Rate of {user_currency} to Bitcoin is: {i['rate']}"
        if user_currency not in list_of_cur:
            return 'Don\'t know'

    @staticmethod
    def db_creator():
        """
        Создает базу данных с помощью sqlite3
        :return: базу данных в диреткории проекта с названием 'btc_rate.db'
        """
        connection = sqlite3.connect('btc_rate.db')
        cursor = connection.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS BTC_Rate
                        (id INTEGER PRIMARY KEY AUTOINCREMENT, Date TEXT, Currency FLOAT, Rate FLOAT);''')
        connection.commit()
        connection.close()


    def db_insert(self):
        """
        Заполняет первый вариант базы данных актуальной на сегодня информацией по курсу BTC.
        :return: заполненную базу 'btc_rate.db'
        """
        connection = sqlite3.connect('btc_rate.db')
        cursor = connection.cursor()
        print("Connect SQLite")

        api = ('https://bitpay.com/api/rates')
        res = self.check_con(api)
        json_data = res.json()
        counter = 0
        actual_date = res.headers['Date']

        for i in json_data:
            counter += 1
            rate = i['rate']
            cur = i['code']
            cursor.execute("""INSERT INTO BTC_Rate
                                (Date, Currency, Rate)
                                VALUES (?, ?, ?);""", (actual_date, cur, rate))
        connection.commit()
        print("Insert is done")

        cursor.close()

    def db_insert_2(self):
        """
        Заполняет второй вариант базы данных, которая была создана с помощью model.
        Для работы потребуется импортировать session из DB_Utils и BTC из model
        :return: заполненную базу данных 'btc_rate_tod.db' актуальной инфой по курсу BTC
        """
        session = get_session()
        api = ('https://bitpay.com/api/rates')
        res = self.check_con(api)
        json_data = res.json()
        counter = 0
        actual_date = res.headers['Date']
        for i in json_data:
            counter += 1
            rate = i['rate']
            cur = i['code']
            btc = BTC(Date=actual_date, Currency=cur, Rate=rate)
            session.add(btc)
        else:
            session.commit()


a = Bitcoin_rate('usd')


