from model import BTC
from werkzeug.exceptions import abort


def get_cur(currency):
    cur = BTC.query.filter_by(Currency = currency).one()
    if cur is None:
        abort(404)
    else:
        return cur


def get_record_by_id(record_currency, session, model):
    record = session.query(model).filter_by(Currency=record_currency).one()
    if record is None:
        abort(404)
    else:
        return record