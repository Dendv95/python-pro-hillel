import sqlite3
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from variables import URL, URI_DB
# from model import base

def get_db_connection():
    connection = sqlite3.connect(URL)
    connection.row_factory = sqlite3.Row
    return connection


def get_session():
    engin = create_engine(URI_DB)
    base.metadata.bind = engin
    db_session = sessionmaker(bind=engin)
    return db_session()