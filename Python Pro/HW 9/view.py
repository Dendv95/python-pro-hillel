from model import BTC
from app import app, db
from flask import render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy

from sqlalchemy import or_

from webargs.flaskparser import use_kwargs
from webargs import fields

from variables import DEBUG
from DB_pop import get_cur

@app.route("/")
def index():
    return render_template('index.html')

@app.route('/cur/list')
def list_currency():
    cur = BTC.query.all()

    return render_template('BTC/list.html', cur=cur)

@app.route('/cur/rates')
@use_kwargs(
    {
        'currency': fields.Str(required=True)
    },
    location='query'
)
def detail_cur(currency):
    cur = get_cur(currency)
    return render_template('BTC/detail.html', cur=cur)

@app.route('/btc/search')
@use_kwargs(
    {
        'currency': fields.Str(required=True)
    },
    location='query'
)
def search_cur(currency):
    """
        SELECT *
        FROM Currency
        WHERE Currency = '' OR Rate = '';
    """
    cur = BTC.query.filter(or_(BTC.Currency == currency.upper(), BTC.Rate == currency.upper())).all()

    return render_template('BTC/search_res.html', cur=cur)


if __name__ == '__main__':
    app.run(debug=DEBUG)