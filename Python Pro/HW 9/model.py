from app import db
# from sqlalchemy import Column, Integer, String, DateTime
# from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy import create_engine
# from datetime import datetime
# from variables import URI_DB
# base = declarative_base()


class BTC(db.Model):

    __tablename__ = 'BTC_Rate'

    id = db.Column(db.Integer, primary_key=True)
    Date = db.Column(db.String(100), nullable=False)
    Currency = db.Column(db.String(100), nullable=False)
    Rate = db.Column(db.Integer, nullable=False)

    def __str__(self):
        return f'Today rate {self.Currency} to BTC us {self.Rate}'

# engin = create_engine(URI_DB)
# base.metadata.create_all(engin)