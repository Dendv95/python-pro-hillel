from flask import Flask, render_template

from variables import  DEBUG

app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')


@app.route("/avr_data")
def get_avr_data():
    f1 = open('hw.csv')
    a = f1.read()
    b = a.split()
    kg_list = []
    cm_list = []
    pound = 0.409517
    inch = 2.54
    for i in b:
        i = i.replace(',', '')
        index = i.find('.')
        if index == 3:
            res = float(i) * pound
            kg_list.append(res)

        elif index == 2:
            res = float(i) * inch
            cm_list.append(res)
    avg_hei = (sum(cm_list) / len(cm_list))
    avg_wei = (sum(kg_list) / len(kg_list))
    average = f'Student average weight is {avg_wei} kg and average height is {avg_hei} cm'
    return render_template('student/avarage.html', average = average)


@app.route("/requirements")
def get_req():
    file = open('requirements.txt', 'r', encoding='utf-16', newline='\r\n')
    html = file.readlines()
    return render_template('student/requirements.html', html = html)


app.run(debug=DEBUG)
