import sqlite3

from flask import Flask, render_template

from variables import URL, DEBUG

app = Flask(__name__)

def get_db_connection():

    connection = sqlite3.connect(URL)
    connection.row_factory = sqlite3.Row
    return connection

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/unique_name")
def get_unique_name():
    connection = get_db_connection()
    uniq_name = connection.execute('SELECT DISTINCT FirstName FROM customers GROUP BY FirstName;').fetchall()
    connection.close()
    return render_template('uniq_name.html', uniq_name = uniq_name)

app.run(debug=DEBUG)