class Student:


    def __init__(self, name, surname, grade= 'Student'):
        self.name = name
        self.surname = surname
        self.grade = grade


    def add_grade(self, grade):
        if not isinstance(grade, (str)):
            raise TypeError
        self.grade = grade
        return self.grade

    def display(self):
        return f'Grade of {self.name} {self.surname} is {self.grade}'


st1 = Student('Nick', 'Fury', 'Student')
print(st1.grade)
print(st1.name)
print(st1.surname)
print(st1.display())
print(st1.add_grade('Bachelor'))
print(st1.display())
st2 = Student('Alex', 'Bern', 'Doctor')
st3 = Student('Jey', 'Rox')
print(st3.add_grade('Student'))
print(st3.add_grade('Magistr'))
print(st3.display())


class Group:

    qty = 0
    student_list = []

    def __init__(self, name):
        self.name = name

    def add_student(self, student):

        if isinstance(student, Student):
            self.student_list.append(student)
        self.qty = len(self.student_list)
        return self.student_list, self.qty


    def display_group(self):

        return f'In our group {self.name} are {self.qty} persons!'


gr1 = Group('A')
print(gr1.display_group())
print(gr1.add_student(st1))
print(gr1.add_student(st2))
print(gr1.display_group())
print(gr1.add_student(st3))
print(gr1.display_group())
for i in gr1.student_list:
    print(i.name, i.grade, i.surname)